package com.cxs.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 文件分片表
 * @TableName sys_chunk_record
 */
@TableName(value ="sys_chunk_record")
@Data
@Accessors(chain = true)
public class SysChunkRecord implements Serializable {
    /**
     * kid
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String kid;

    /**
     * 文件名称
     */
    private String chunkFileName;

    /**
     * 文件存放路径
     */
    private String chunkFilePath;

    /**
     * 文件md5
     */
    private String fileMd5;

    /**
     * 文件分片md5
     */
    private String currentChunkMd5;

    /**
     * 分片大小
     */
    private Integer chunkSize;

    /**
     * 第几块分片
     */
    private Integer currentChunk;

    /**
     * 总分片数量
     */
    private Integer chunks;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}