package com.cxs.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/*
 * @Project:file-upload-senior
 * @Author:cxs
 * @Motto:放下杂念,只为迎接明天更好的自己
 * */
@Data
public class FileUploadDTO {
    /**
     * 是否是分片上传
     */
    @NotNull(message = "是否分片不能为空")
    private Boolean chunkFlag;

    /**
     * 文件
     */
    @NotNull(message = "文件不能为空")
    private MultipartFile file;

    /**
     * 文件名
     */
    @NotBlank(message = "文件名不能为空")
    private String name;

    /**
     * 文件总大小
     */
    private Long size;

    /**
     * 文件md5
     */
    @NotBlank(message = "文件md5不能为空")
    private String fileMd5;

    /**
     * 文件类型
     */
    private String type;

    /**
     * 当前分片
     */
    private Integer currentChunk;

    /**
     * 分片长度
     */
    private Integer chunkSize;

    /**
     * 总分片数量
     */
    private Integer chunks;

    /**
     * 分片文件md5
     */
    private String currentChunkMd5;
}
