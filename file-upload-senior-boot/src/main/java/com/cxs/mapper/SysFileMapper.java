package com.cxs.mapper;

import com.cxs.model.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author DELL
* @description 针对表【sys_file(文件表)】的数据库操作Mapper
* @createDate 2022-12-10 14:14:15
* @Entity com.cxs.model.SysFile
*/
public interface SysFileMapper extends BaseMapper<SysFile> {

}




