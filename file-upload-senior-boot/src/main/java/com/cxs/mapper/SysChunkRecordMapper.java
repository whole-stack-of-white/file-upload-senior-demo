package com.cxs.mapper;

import com.cxs.model.SysChunkRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author DELL
* @description 针对表【sys_chunk_record(文件分片表)】的数据库操作Mapper
* @createDate 2022-12-10 14:14:15
* @Entity com.cxs.model.SysChunkRecord
*/
public interface SysChunkRecordMapper extends BaseMapper<SysChunkRecord> {

}




