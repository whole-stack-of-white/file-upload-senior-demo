package com.cxs.service;

import com.cxs.model.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author DELL
* @description 针对表【sys_file(文件表)】的数据库操作Service
* @createDate 2022-12-10 14:14:15
*/
public interface SysFileService extends IService<SysFile> {

}
