package com.cxs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cxs.model.SysChunkRecord;
import com.cxs.service.SysChunkRecordService;
import com.cxs.mapper.SysChunkRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author DELL
* @description 针对表【sys_chunk_record(文件分片表)】的数据库操作Service实现
* @createDate 2022-12-10 14:14:15
*/
@Service
public class SysChunkRecordServiceImpl extends ServiceImpl<SysChunkRecordMapper, SysChunkRecord>
    implements SysChunkRecordService{

}




