package com.cxs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cxs.model.SysFile;
import com.cxs.service.SysFileService;
import com.cxs.mapper.SysFileMapper;
import org.springframework.stereotype.Service;

/**
* @author DELL
* @description 针对表【sys_file(文件表)】的数据库操作Service实现
* @createDate 2022-12-10 14:14:15
*/
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile>
    implements SysFileService{

}




