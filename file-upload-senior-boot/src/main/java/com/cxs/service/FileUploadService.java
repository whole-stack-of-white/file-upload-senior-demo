package com.cxs.service;

import com.cxs.dto.FileUploadDTO;
import com.cxs.utils.Result;

/*
 * @Project:file-upload-senior
 * @Author:cxs
 * @Motto:放下杂念,只为迎接明天更好的自己
 * */
public interface FileUploadService {

    /**
     * 分片上传
     * @param dto
     * @param result
     */
    void chunkFileUpload(FileUploadDTO dto, Result result);

    /**
     * 单文件上传
     * @param dto
     * @param result
     */
    void singleFileUpload(FileUploadDTO dto, Result result);
}
