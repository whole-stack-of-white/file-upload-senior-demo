package com.cxs.service;

import com.cxs.model.SysChunkRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author DELL
* @description 针对表【sys_chunk_record(文件分片表)】的数据库操作Service
* @createDate 2022-12-10 14:14:15
*/
public interface SysChunkRecordService extends IService<SysChunkRecord> {

}
