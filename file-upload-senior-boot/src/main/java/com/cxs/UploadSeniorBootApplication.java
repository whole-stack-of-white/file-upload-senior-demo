package com.cxs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * @Project:file-upload-senior
 * @Author:cxs
 * @Motto:放下杂念,只为迎接明天更好的自己
 * */
@SpringBootApplication
@MapperScan(basePackages = "com.cxs.mapper")
public class UploadSeniorBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(UploadSeniorBootApplication.class, args);
    }
}
