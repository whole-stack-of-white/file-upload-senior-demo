import axios from 'axios'
const service = axios.create({
  // 请求时携带cookie
  withCredentials: true, 
  // 超时20s
  timeout: 200000
})


service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  response => {
    const { status, data } = response;
    if(status === 200) {
      if(data.code && data.code !== 200) {
        console.log(data.msg || response)
      } else {
        return data;
      }
    } else {
      console.log(response)
    }
  },
  error => {
    return Promise.reject(error)
  }
)

export default service
