import request from './request'

/**
 * 文件上传
 * @param {*} data 
 * @returns 
 */
export const fileUpload = (data) => {
    const uploadUrl = 'http://127.0.0.1:2022/upload'
    return request({
        url: uploadUrl,
        method: 'POST',
        data
    })
}

/**
 * 检查文件是否存在
 * @param {*} fileMd5Id 
 * @returns 
 */
export const checkFileExist = (fileMd5Id) => {
    return request({
        url: 'http://127.0.0.1:2022/checkFileExist',
        method: 'GET',
        params: {
            fileMd5Id
        }
    })
}