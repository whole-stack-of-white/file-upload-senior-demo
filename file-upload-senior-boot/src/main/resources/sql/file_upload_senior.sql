create table sys_chunk_record
(
    kid               varchar(36)  not null comment 'kid'
        primary key,
    chunk_file_name   varchar(255) not null comment '文件名称',
    chunk_file_path   varchar(255) not null comment '文件存放路径',
    file_md5          varchar(100) not null comment '文件md5',
    current_chunk_md5 varchar(100) not null comment '文件分片md5',
    chunk_size        int          not null comment '分片大小',
    current_chunk     int          not null comment '第几块分片',
    chunks            int          not null comment '总分片数量',
    create_time       datetime null comment '创建时间',
    constraint idx_chunk_file_md5
        unique (current_chunk_md5)
) comment '文件分片表';

create table sys_file
(
    kid         varchar(36)  not null comment 'kid'
        primary key,
    file_name   varchar(255) not null comment '文件名称',
    extension   varchar(255) null comment '文件扩展名',
    file_size   bigint null comment '文件大小',
    file_type   varchar(255) not null comment '文件类型',
    file_path   varchar(255) not null comment '文件存放路径',
    file_md5    varchar(100) not null comment '文件md5',
    remark      varchar(500) null comment '备注',
    create_time datetime null comment '创建时间',
    constraint idx_file_md5
        unique (file_md5)
) comment '文件表';

